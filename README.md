# My Website (www.lwalldevelopment.com)

This is the repo of my live website which can be found <a href="http://www.lwalldevelopment.com/">here</a>. Features the use of:

<ul>
  <li>HTML5</li>
  <li>CSS3/SASS</li>
  <li>Bootstrap</li>
  <li>JavaScript/jQuery</li>
  <li>FontAwesome</li>
</ul>

<img src="images/Screen Shot 2018-05-29 at 8.37.08 AM.png">

Thanks to <a href="http://nicklosacco.com/miter/">Nicholas Losacco</a> for the extravagent Miter Bold typeface used in the headers.

